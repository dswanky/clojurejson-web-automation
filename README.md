# automate-shipping

~insert-name-here is a functional toolset for automating tasks. While initially focusing on web-browser automation, it can realistically be used to automate any rule-based task. The toolset includes various complementary parts designed to make automation tools that are both easy to create and easy to use. ~insert-name-here is also designed to be extended, easily so with Clojure, Java, or any other JVM language. I am currently developing the following parts of ~insert-name-here:

* Automation configuration language
A combinatation between a configuration file and programming language. This is currently a JSON based language due to the ease of implementation, but syntactically this was a dire mistake. This format will be changed to a lisp-like language. The power of the configuration language comes in its simplicity. As much complexity as possible should be hidden in the "parent" language code rather than in the configuration code.

* Web automation
Utilizing Etaoin (https://github.com/igrishaev/etaoin), a web browser automation wrapper/library has been built to assist with ~insert-name-here's original purpose. While ~insert-name-here's purpose has broadened, the benefit is a powerful web automation framework available from your configuration scripts.

* Terminal UI Framework
Utilizing the Laterna Java library (https://github.com/mabe02/lanterna), a library for simple UIs in the terminal was also created. A terminal was chosen for its ease of development, while keeping most functionality needed in a GUI. Configuration scripts can easily build UIs and attach functionality to button presses, while the internal codebase can be expanded to simplify UI tasks further based on your needs. 

* AES Encryption
An simple AES encryption library was created to make file encryption and string encryption simpler. The primary motivator for this was for caching passwords. PGP was my preference, but the average user does not use PGP. They simply use passwords. I used the default Java library for the actual AES algorithm and tools.

* Password Store Encryption
Just an expansion of the above, but with easy access to passwords programmed in. 

## Installation

Most easily run in Emacs Cider: https://github.com/clojure-emacs/cider. Otherwise, download leiningen from https://leiningen.org/run . After installation run "lein uberjar" in the root folder of the project. This will create a jar file you can run.

## Usage

On linux, launch the jar with this command:

    $ java -jar automate-shipping-0.1.0-standalone.jar [args]
	
Upon launching one of two things will happen depending on how you launch it. If you launch it in a compatible terminal then a text-based interface will appear. If you launch the application in a non-compatible terminal (such as one that is part of your IDE) then a Java Swing terminal will open with the text-based interface. (see https://github.com/mabe02/lanterna/blob/master/docs/contents.md for more information). The two should operate similarly.

Navigate via arrow keys, press enter on a button or text field to execute its associated action.

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2021 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.

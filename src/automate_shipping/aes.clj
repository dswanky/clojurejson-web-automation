(ns automate_shipping.aes
  (:gen-class))

(import (javax.crypto Cipher SecretKeyFactory SecretKey)
        (javax.crypto.spec PBEKeySpec SecretKeySpec IvParameterSpec)
        (java.security SecureRandom)
        (org.apache.commons.codec.binary Base64))

(use '[automate_shipping.utility])

;Inspired by https://www.baeldung.com/java-aes-encryption-decryption

(def salt "SNQ4Uh$Af6m@Gwt8r!%GOkMuPsSdsHSJI##y5*Z&Y^smk50^d!JwuRnqnd^x0Hio")

(defn get-key-from-password
  "Generate the secret key given a password and a salt"
  [password salt]
  (let [secretKeyFactory (SecretKeyFactory/getInstance "PBKDF2WithHmacSHA256")
        keySpec (PBEKeySpec. (.toCharArray password) (.getBytes salt) 100000 256)
        secretKey (SecretKeySpec. (.getEncoded (.generateSecret secretKeyFactory keySpec)) "AES")]
    secretKey))

(defn generate-initialization-vector
  "Generate an initalization vector for the AES algorithm"
  []
  (let [iv (byte-array 16)]
    (.nextBytes (SecureRandom. ) iv)
    (IvParameterSpec. iv)))

(defn generate-cipher
  "Generate an AES cipher"
  []
  (Cipher/getInstance "AES/CBC/PKCS5Padding"))

(defn set-cipher-mode
  "Set the cipher's mode"
  [cipher mode secret-key initialization-vector]
  (.init cipher mode secret-key initialization-vector))

(defn set-encryption-mode
  "Set the cipher in encryption mode"
  [cipher secret-key initialization-vector]
  (set-cipher-mode cipher (Cipher/ENCRYPT_MODE) secret-key initialization-vector))

(defn set-decryption-mode
  "Set the cipher in decryption mode"
  [cipher secret-key initialization-vector]
  (set-cipher-mode cipher (Cipher/DECRYPT_MODE) secret-key initialization-vector))

(defn pass-alphabet
  "Generates a basic password alphabet"
  []
  (let [lower "abcdefghijklmnopqrstuvwxyz"
        upper (.toUpperCase lower)
        nums "0123456789"
        symbols "!@#$%^&*()"]
    (char-array (str lower upper nums symbols))))

(defn create-random-char
  "Create a secure random char"
  [secureRandom alphabet]
  (nth alphabet (.nextInt secureRandom (count alphabet))))

(defn create-random-string
  "Create a secure random string of characters from an alphabet"
  ([length alphabet]
   (let [secureRandom (SecureRandom.)]
     (loop [counter 0
            result '()]
       (if (< counter length)
         (recur (inc counter)
                (cons (create-random-char secureRandom alphabet) result))
         (clojure.string/join result)))))
  ([length]
   (create-random-string length (pass-alphabet))))

(defn password-crypt-string-helper
  "Helper for encrypting/decrypting strings with a password"
  ([crypt-func plaintext password salt initial-vector]
  (let [secret-key (get-key-from-password password salt)
        cipher (generate-cipher)]
    (crypt-func cipher secret-key initial-vector)
    (.doFinal cipher (string-to-bytes plaintext))))
  ([crypt-func plaintext password salt]
   (let [initial-vector (generate-initialization-vector)]
     (list initial-vector (password-crypt-string-helper crypt-func plaintext password salt initial-vector)))))

;; Need to save initialization vector in the file, and use that.
;; Create a "decrypt from file" and "encrypt to file" function to help
(defn encrypt-string-with-password
  "Encrypt a plaintext string with a password using 256 AES"
  ([plaintext password salt initial-vector]
   (password-crypt-string-helper set-encryption-mode plaintext password salt initial-vector))
  ([plaintext password salt]
   (password-crypt-string-helper set-encryption-mode plaintext password salt)))

(defn decrypt-string-with-password
  "Decrypt a plaintext string with a password using 256 AES"
  ([plaintext password salt initial-vector]
   (password-crypt-string-helper set-decryption-mode plaintext password salt initial-vector))
  ([plaintext password salt]
   (password-crypt-string-helper set-decryption-mode plaintext password salt)))

;; The point of this is to make all data unencryptable when given only the IV
;; The IV is stored in plain text.
;; This generates gibberish for the first block so all data is safe
(defn generate-first-block
  "Generate a gibberish first block"
  []
  (let [block-size-bytes (/ 128 8)]
    (fill-garbage "" block-size-bytes)))

(defn format-encrypted-file
  "Generate the format for the encrypted file"
  [contents]
  (str (generate-first-block) contents))

(defn unformat-encrypted-file
  "Take a formatted file and return a list of the parts"
  [contents]
  (let [iv (subs contents 0 16)
        encrypted-content (subs contents 16)] 
    (list iv encrypted-content)))

(defn encrypt-file-with-password
  "Encrypt a file with a password using 256 AES"
  ([file-name password salt initial-vector]
   (let [file-contents (slurp file-name)]
     (spit file-name
           (str (bytes-to-string (.getIV initial-vector))
                (bytes-to-string (encrypt-string-with-password
                                  (format-encrypted-file file-contents)
                                  password
                                  salt
                                  initial-vector))))))
  ([file-name password salt]
   (encrypt-file-with-password file-name password salt (generate-initialization-vector))))

(defn decrypt-file-with-password
  "Decrypt a file with a password using 256 AES"
  [file-name password salt]
  (let [file-contents (slurp file-name)
        split-file-contents (unformat-encrypted-file file-contents)
        iv (IvParameterSpec. (string-to-bytes (first split-file-contents)))
        encrypted-content (second split-file-contents)
        decrypted-content (decrypt-string-with-password encrypted-content
                                                        password
                                                        salt
                                                        iv)]
    (subs (bytes-to-string decrypted-content) 16))) ;;Skip the garbage first block

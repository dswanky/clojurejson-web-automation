(ns automate_shipping.core
  (:gen-class))

(import 'com.googlecode.lanterna.terminal.Terminal)
(use '[etaoin.api])
(use '[flatland.ordered.map])
(use '[automate_shipping.utility])
(use '[automate_shipping.web_automation])
(use '[automate_shipping.aes])
(use '[automate_shipping.password_file])
(use '[automate_shipping.terminal_ui])
(import java.io.Console)

(import 'com.googlecode.lanterna.terminal.DefaultTerminalFactory)
(import 'com.googlecode.lanterna.terminal.swing.SwingTerminal)
(import 'com.googlecode.lanterna.screen.Screen)
(import 'com.googlecode.lanterna.screen.Screen)
(import 'com.googlecode.lanterna.input.KeyStroke)
(import 'com.googlecode.lanterna.input.KeyType)
(import 'com.googlecode.lanterna.TextColor$ANSI)
(import 'java.awt.GraphicsEnvironment)
(import 'java.awt.Font)
(import 'java.awt.Toolkit)


(def function-mapping-loc "function-mapping.json")
(def instruction-sequence "instructions.json")
(declare process-instruction)
(declare process-instructions)

(defn filter-func-config
  "Checks a map of alias-function relations"
  [func-map]
  (let [filtered-map-coll (filter
                           (fn [[k v]] (function-name? v))
                           func-map)]
    (into (ordered-map) filtered-map-coll)))

(defn get-function-mappings
  "Pulls json from the function-mapping config"
  [config]
  (let [jmap (read-json config)]
     (filter-func-config jmap)))

(def special-instructions {"if" "s-if"
                           "while" "s-while"
                           "loop" "s-loop"
                           "when" "s-when"
                           "run-file" "s-run-file"})

(def func-special-end-chars [\_])

(defn verify-alias
  "See if a function alias is mapped"
  [a-symbol mapped-symbols]
  (let [trimmed-a-symbol (remove-end-chars a-symbol func-special-end-chars)]
      (or (contains? mapped-symbols trimmed-a-symbol)
      (contains? special-instructions trimmed-a-symbol))))

(defn verify-aliases
  "Just see if a collection of function aliases is wholly correct"
  [symbols mapped-symbols]
  (let [verified (verify-alias (first symbols) mapped-symbols)]
    (if verified
      (if (empty? (rest symbols))
        verified
        (verify-aliases (rest symbols) mapped-symbols))
      nil)))

(defn verify-instruction-data
  "Verifies functios have definitions and more soon"
  [instructions function-mappings]
  (let [aliases (keys instructions)]
    (verify-aliases aliases function-mappings)))


(defn get-instruction-data
  "Gets the list of commands to perform"
  [config function-mappings]
  (let [instructions (read-json config)
        instruction-v (verify-instruction-data instructions function-mappings)]
    (if instruction-v
      instructions
      (println "An alias is not attached to a defined function"))))

(defn process-function-argument
  "Process function argument, running it in the case it is a function"
  [mapping arg]
  (if (map? arg)
    (process-instructions arg mapping)
    arg))

(defn process-function-arguments
  "Process function arguments, running parameters if they are functions"
  [mapping args]
  (map (partial process-function-argument mapping)
       (mapcat make-sure-sequence
               args)))
;; Our input is always initially processed as a single argument. If it is an array of arguments in config, it is still a single argument
;; But we don't want that. We want arrays to be unravelled (flattened) one-level deep so that functions with multiple arguments can run
;; Basically, we want to flatten an array only one level, yet also not exclude non-arrays
;; normal concat will exclude nonarrays. mapcat is like concat, but it performs a function on every subsequent argument
;; All we are doing is turning non-arrays into single-element arrays so that they are processed like a normal argument

(defn get-function-from-symbol
  "Given a symbol (or string), derive the matching function"
  [fsymbol]
  (resolve (symbol fsymbol)))

(defn run-function-symbol
  "Given a symbol (or string), run the matching function"
  [fsymbol mapping args]
  (let [f (get-function-from-symbol fsymbol)]
    (when (some? f)
      (apply f (process-function-arguments mapping (make-sure-sequence args)))))) ;; This may seem complicated but it's not, see above

(defn return-value
  "Return the value of the expression. If it's a function, run it. Otherwise just return it"
  [mapping e]
  (if (map? e)
    (process-instructions e mapping)
    e))

(def var-table (atom {}))

(defn add-var-to-table
  "Add a variable to the var table"
  [k v]
  (do
    (swap! var-table assoc k v)
    (get @var-table k)))

(defn get-var-from-table
  "Retrieve the value of a variable from the var table"
  [k]
  (get @var-table k))

(defn get-mapping
  "Given an alias, get the function symbol"
  [falias mapping]
  (get mapping falias))

(defn process-instruction
  "Run a single instruction"
  [instruction mapping]
  (let [falias (first (keys instruction))
        args (first (vals instruction))
        falias-t (remove-end-chars falias func-special-end-chars)
        f (get mapping falias-t)]
    (if (contains? special-instructions falias-t)
      (apply (get-function-from-symbol (get special-instructions falias-t)) mapping args)
      (run-function-symbol f mapping args))))

(defn process-instruction-keys
  "Takes the keys and uses those to process the map"
  [i-keys instructions mapping]
  (let [next (first i-keys)
        i-rest (rest i-keys)
        next-instruction {next (get instructions next)}]
    (if (not-empty i-rest)
      (cons (process-instruction next-instruction mapping)
            (process-instruction-keys i-rest instructions mapping))
      (process-instruction next-instruction mapping))))

(defn process-instructions
  "Actually run the instructions"
  [instructions mapping]
  (process-instruction-keys (keys instructions) instructions mapping))

(defn s-if
  "Special symbol version of if statement"
  [mapping cond x y]
  (if (return-value mapping cond)
    (return-value mapping x)
    (return-value mapping y)))

(defn s-when
  "Special symbol version of when statement"
  [mapping cond f]
  (s-if mapping cond f nil))

(defn s-while
  "Special symbol version of while statement"
  [mapping cond f]  
  (loop [return-vals '()]
    (if (return-value mapping cond)
      (recur (cons (return-value mapping f) return-vals))
      return-vals)))

(defn s-loop
  "Run expression n times"
  [mapping n f]
  (loop [counter 0 result '()]
    (if (< counter n)
      (recur (inc counter) (cons (return-value mapping f) result))
      result)))

(defn s-run-file
  "Run a file as a function"
  [mapping file-name]
    (process-instructions (get-instruction-data file-name mapping) mapping))

;; DONE: Load in actions from a config
;; DONE: Read in a JSON mapping that maps keywords to functions
;; DONE: Allow control statements
;;; * Parse as functions. For example:
;; {"if" : [{"<" : [x y]}, {"do" : "this"}, {"do" : "that"}]}
;; Look at "process-instructions". Add "special symbol" check for things like if statements
;; These "special symbol" functions will operate outside the norm, having things like lazy evaluation
;; Restructure the query parser to take singular arguments at a time using & rather than an array
;; The reason for this is the function parser removes one layer of array, so it's not being passed
;; as an array

;; Check why when you run "lein uberjar" and run the jar you get an error like an alias is not defined
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [function-mappings (get-function-mappings function-mapping-loc)]
                                        ;(s-run-file function-mappings instruction-sequence)
    (initialize-terminal-ui)))

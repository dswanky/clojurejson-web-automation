(ns automate_shipping.password_file
  (:gen-class))

(require '[automate_shipping.ordered_json :as json])
(use '[automate_shipping.utility])
(use '[automate_shipping.aes])
(use '[flatland.ordered.map])

;; Format is {website : {username : password}}
;; We will only support one username/password per website for now
;; But this gives the ability to expand in the future

(defn create-cred-file
  [file-name]
  (write-json file-name "{}"))

(defn read-creds
  "Read and decrypt the credentials from file"
  [file-name password salt]
  (try
    (json/read-str (decrypt-file-with-password file-name password salt))
    (catch Exception e
      (str e "Error, file is not properly formatted, the file is corrupt, or the password is wrong."))))

(defn add-creds
  "Add a credential to the credential set"
  [file-name file-pass file-salt website username password]
  (let [added-table (assoc
         (read-creds file-name file-pass file-salt)
         website {username password})]
    (if added-table
      (do (write-json file-name (into (hash-map) added-table))
          (encrypt-file-with-password file-name file-pass file-salt))
      (do (create-cred-file file-name)
          (add-creds file-name file-pass file-salt website username password)))))

(defn get-username-password
  "Get a single username/password given a website"
  [file-name password salt website]
  (get (read-creds file-name password salt) website))

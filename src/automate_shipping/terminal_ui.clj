(ns automate_shipping.terminal_ui
  (:gen-class))

(use '[automate_shipping.utility])
(use '[flatland.ordered.map])
(import 'com.googlecode.lanterna.terminal.DefaultTerminalFactory)
(import 'com.googlecode.lanterna.terminal.swing.SwingTerminal)
(import 'com.googlecode.lanterna.screen.Screen)
(import 'com.googlecode.lanterna.screen.Screen)
(import 'com.googlecode.lanterna.input.KeyStroke)
(import 'com.googlecode.lanterna.input.KeyType)
(import 'com.googlecode.lanterna.TextColor$ANSI)
(import 'java.awt.GraphicsEnvironment)
(import 'java.awt.Font)
(import 'java.awt.Toolkit)

(defrecord Navigation-Connections [up down left right])
(defrecord Term-Color [foreground background])
(defrecord Button-Options [f select-color])
(defrecord Text-Field-Options [f select-color width])
(defrecord Graphics-Element [screen type text region color navigation-connections options])


(def graphic-elements (atom (ordered-map)))

(defn swap-graphic-element
  [graphic-id graphic]
  (swap! graphic-elements assoc (keyword (str "id_" graphic-id)) graphic))


(defn add-graphic-element
  "Add the graphic element to the graphic-elements table"
  [element]
  (let [elements-keys (keys @graphic-elements)
        last-key (when (some? elements-keys) (last elements-keys))
        last-element-id (if (some? last-key)
                          (Integer/parseInt (last (clojure.string/split (str last-key) #"_")))
                          0)
        cur-element-id (inc last-element-id)]
    (swap-graphic-element cur-element-id element)
    cur-element-id))

(defn get-graphic
  [graphic-id]
  (let [true-id (str "id_" graphic-id)]
    (get @graphic-elements (keyword true-id))))

(defn start-terminal
  "Start the terminal screen"
  []
  (let [screen (.createScreen (DefaultTerminalFactory.))]
    (.startScreen screen)
    screen))

(defn stop-terminal
  "Stop the terminal screen"
  [screen]
  (.stopScreen screen)
  screen)

(defn get-position-info
  "Get color and char info at a position"
  [screen position]
  (let [term-char (.getFrontCharacter screen (first position) (second position))
        character (.character term-char)
        foreground (.foregroundColor term-char)
        background (.backgroundColor term-char)]
    {:char character
     :foreground foreground
     :background background}))

(defn get-region-info
  "Get a list of position infos from a region"
  [screen positionStart positionStop]
  (loop [region '()
         curX (first positionStart)
         curY (second positionStop)]
    (if (and (< curX (first positionStop)) (< curY (second positionStop)))
      (recur (cons region (get-position-info screen (list curX curY)))
             (inc curX)
             (inc curY))
      region)))

;; Only reason I'm using this is to be able to dispatch by type
;; This is faster than multi-methods, and a bit simpler as well


(defn change-graphic-object
  [graphic & {:as to-change}]
  (let [k (keys to-change)]
    (loop [keys-to-change k
           graphic-to-change graphic]
      (if (> (count keys-to-change) 0)
        (recur (rest keys-to-change)
               (assoc graphic-to-change (first keys-to-change) (get to-change (first keys-to-change))))
        graphic-to-change))))

(defn change-graphic-element
  "Return a record that is the same as graphic, but with to-change elements changed"
  [graphic-id & {:as to-change}]
  (swap-graphic-element graphic-id (partial-apply `(~change-graphic-object
                                                    ~(get-graphic graphic-id))
                                                  to-change))
  graphic-id)

(declare write-element)
(declare apply-text-with-width)

(defn update-graphic-element
  "Update the graphic record and write it to the screen"
  [graphic-id & {:as to-change}]
  (write-element graphic-id (get-graphic (partial-apply `(~change-graphic-element ~graphic-id)
                                           to-change))))

(declare create-custom-graphic)

(defprotocol ScreenWriter
  "Write to terminal screen"
  (write-text [graphic-id screen region text color]
    [screen region text color]
    [gid g]
    [g])
  (write-button [graphic-id screen region text color navigation f]
    [screen region text color navigation f]
    [gid g]
    [g])
  (write-input-field [graphic-id screen region text color navigation options]
    [screen region text color navigation f]
    [gid g]
    [g]))



;; Start refactor block
;; Just add some helper functions to reduce redundancy
(extend-type java.lang.Number
  ScreenWriter
  (write-text
    ([graphic-id screen region text color]
     (let [textGraphics (.newTextGraphics screen)]
       (.setForegroundColor textGraphics (:foreground color))
       (.setBackgroundColor textGraphics (:background color))
       (.putString textGraphics (first region) (second region) text)
       (change-graphic-element graphic-id :type :text
                               :text text
                               :region region
                               :color color)))
    ([graphic-id graphic]
     (write-text graphic-id
                 (:screen graphic)
                 (:region graphic)
                 (:text graphic)
                 (:color graphic))))

  (write-button
    ([graphic-id screen region text color navigation options]
     (change-graphic-element graphic-id :screen screen :region region
                             :text text :color color :type :button
                             :navigation-connections navigation :options options)
     (write-text screen region text color))
  ([graphic-id graphic]
    (write-button graphic-id (:screen graphic ) (:region graphic) (:text graphic)
                  (:color graphic) (:navigation-connections graphic) (:options graphic))))

  (write-input-field
    ([graphic-id screen region text color navigation options]
     (change-graphic-element graphic-id :screen screen :region region
                             :text text :color color :type :input
                             :navigation-connections navigation :options options)
     (write-text screen region text color)
     (apply-text-with-width graphic-id text))
    ([graphic-id graphic]
     (write-input-field graphic-id (:screen graphic) (:region graphic)
                        (:text graphic) (:color graphic)
                        (:navigation-connections graphic) (:options graphic)))))


(extend-type java.lang.Object
  ScreenWriter
  (write-text
    [screen region text color]
    (let [graphic-id (add-graphic-element (Graphics-Element. screen :text text region
                                                             color
                                                             (Navigation-Connections. nil nil nil nil)
                                                             nil))]
      (write-text graphic-id screen region text color)))
  (write-button
    [screen region text color navigation options]
    (create-custom-graphic screen region text color
                           :type :button :navigation-connections navigation :options options))
  (write-input-field
    [screen region text color navigation options]
    (create-custom-graphic screen region text color
                           :navigation-connections navigation
                           :options options)))

(extend-type Graphics-Element
  ScreenWriter
  (write-text
    ([graphic]
    (write-text (:screen graphic)
                (:region graphic)
                (:text graphic)
                (:color graphic))))
  (write-button
     ([graphic]
     (write-button (:screen graphic) (:region graphic) (:text graphic)
                   (:color graphic) (:navigation-connections graphic) (:options graphic))))
  (write-input-field
    ([graphic]
     ((write-input-field (:screen graphic) (:region graphic) (:text graphic)
                 (:color graphic) (:navigation-connections graphic) (:options graphic))))))

(defn create-custom-graphic
  [screen region text color & options]
  (let [graphic-id (write-text screen region text color)]
    (partial-apply `(~update-graphic-element ~graphic-id) options)
    graphic-id))

(defn write-element
  "Write an element based on its type"
  ([graphic-id graphic]
   (let [g-type (:type graphic)]
     (cond
       (= g-type :text) (write-text graphic-id graphic)
       (= g-type :button) (write-button graphic-id graphic))))
  ([graphic]
   (let [g-type (:type graphic)]
     (cond
       (= g-type :text) (write-text graphic)
       (= g-type :button) (write-button graphic)))))

;;end refactor block

(defn apply-text-with-width
  "Edit text based on options"
  [graphic-id new-text]
  (let [graphic (get-graphic graphic-id)]
    (when (contains? (:options graphic) :width)
      (let [width (-> graphic :options :width)
            text-len (count new-text)
            spaces-needed (- width text-len)]
        (when (>= spaces-needed 0)
          (update-graphic-element graphic-id
                                  :text (apply str new-text (repeat spaces-needed " "))))))))

(defn flip-foreground-background
  "Invert the foreground and the background colors"
  [graphic-id]
  (let [graphic (get-graphic graphic-id)
        new-background (-> graphic :color :foreground)
        new-foreground (-> graphic :color :background)]
    (when graphic-id
      (update-graphic-element graphic-id
                              :color (Term-Color. new-foreground new-background)))))

(defn select-graphic
  "Change which element is highlighted"
  [graphic last-graphic]
  (if (nil? graphic)
    last-graphic
    (do (flip-foreground-background graphic)
        (flip-foreground-background last-graphic)
        graphic)))

(defn execute-graphic
  "Execute the graphic's function"
  [graphic]
  ((-> graphic :options :f)))

(defn handle-arrow-input
  "Handle whenever an arrow key is pressed"
  [keyType selected-graphic-id]
  (let [selected-graphic (get-graphic selected-graphic-id)]
    (cond
      (= keyType KeyType/ArrowUp) (select-graphic
                                   (-> selected-graphic :navigation-connections :up) selected-graphic-id)
      (= keyType KeyType/ArrowDown) (select-graphic
                                     (-> selected-graphic :navigation-connections :down) selected-graphic-id)
      (= keyType KeyType/ArrowLeft) (select-graphic
                                     (-> selected-graphic :navigation-connections :left) selected-graphic-id)
      (= keyType KeyType/ArrowRight) (select-graphic
                                      (-> selected-graphic :navigation-connections :right) selected-graphic-id))))

(defn handle-enter-input
  "Handle when the enter key is pressed"
  [selected-graphic-id]
  (list selected-graphic-id
        (execute-graphic (get-graphic selected-graphic-id))))

(defn handle-character-input
  "Handle input when a character is input"
  [in-char selected-graphic-id]
  (let [selected-graphic (get-graphic selected-graphic-id)]
    (when (= (:type selected-graphic) :input)
      (apply-text-with-width selected-graphic-id (str (:text selected-graphic) in-char)))))

(defn handle-backspace
  "Handle when the backspace key is pressed"
  [selected-graphic-id]
  (let [selected-graphic (get-graphic selected-graphic-id)]
    (when (= (:type select-graphic) :input)
      (apply-text-with-width selected-graphic-id (subs (:text select-graphic) 0
                                           (- (count (:text selected-graphic)) 1))))))

(defn input-dispatcher
  "Call the proper functions given a keytype"
  [keyStroke selected-graphic-id]
  (let [keyType (.getKeyType keyStroke)
        arrows [KeyType/ArrowLeft
                KeyType/ArrowRight
                KeyType/ArrowUp
                KeyType/ArrowDown]]
    (cond
      (vector-contains? arrows keyType) (handle-arrow-input keyType selected-graphic-id)
      (= KeyType/Enter keyType) (handle-enter-input selected-graphic-id)
      (= KeyType/Character keyType) (handle-character-input
                                     (.getCharacter keyStroke) selected-graphic-id)
      (= KeyType/Backspace keyType) (handle-backspace selected-graphic-id))))

(defn wait-for-input
  "Block until input is received"
  [selected-graphic-id]
  (let [screen (:screen (get-graphic selected-graphic-id))
        keyStroke (.readInput screen)]
    (input-dispatcher keyStroke selected-graphic-id)))

(defn draw-screen
  "Draw and update the terminal ui"
  [screen]
  (.refresh screen))

(defn input-loop
  "Loop for continuously accepting input"
  [screen selected-graphic-id]
  (draw-screen screen)
  (let [latest-input (wait-for-input selected-graphic-id)]
    (recur screen (if (sequential? latest-input)
                    (first latest-input)
                    latest-input))))

;; Add option for constant-width UI items (e.g. constant-sized input-fields)
;; Probably just needs to replace spaces with text and vice versa when editing
;; More complicated if alignment is wanted
;; Add text input field functions
;; This should include writing, erasing, and possibly submitting
;; Update select code to use select colors rather than flipping the back/foreground
;; Create methods to change "pages" on the screen.
;; Either requries scene "initialization" functions or saving state.
;; Scene initialization requires clearning the graphics hash and re-initializing each screen
;; Saving state requires multiple hash tables, one per screen
(defn initialize-terminal-ui
  "Setup the terminal and wait for input"
  []
  (let [screen (start-terminal)
        text-color (Term-Color. TextColor$ANSI/RED TextColor$ANSI/BLACK)
        button-color (Term-Color. TextColor$ANSI/WHITE TextColor$ANSI/RED)
        input-color (Term-Color. TextColor$ANSI/WHITE TextColor$ANSI/RED)
        button-select-color (Term-Color. TextColor$ANSI/WHITE TextColor$ANSI/RED)
        input-select-color (Term-Color. TextColor$ANSI/RED TextColor$ANSI/WHITE)
        title (write-text screen '(0 0) "Welcome to the UI tEsT" text-color)
        button1 (write-button screen '(0 4) "Button1" button-color nil
                              (Button-Options. (fn [] (write-text screen '(20 20) "u pressed thuh buttin"
                                                                  text-color))
                                               button-select-color))
        button2 (write-button screen '(10 4) "Button2" button-color
                              (Navigation-Connections. nil nil button1 nil)
                              (Button-Options. (fn [] (write-text screen '(20 21) "u pressed thuh uthr buttin"
                                                                  text-color))
                                               button-select-color))
        input1 (write-input-field screen '(0 6) "hi" input-color
                                  (Navigation-Connections. button1 nil button2 nil)
                                  (Text-Field-Options. nil input-select-color 20))]
      
    (update-graphic-element button1 :navigation-connections (Navigation-Connections. nil nil nil button2))
    (update-graphic-element button2 :navigation-connections (Navigation-Connections. nil input1 button1 input1))
    
    (input-loop screen (select-graphic button1 nil))))

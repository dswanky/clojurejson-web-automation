(ns automate_shipping.utility
  (:gen-class))

(require '[automate_shipping.ordered_json :as json])

(defn vector-contains?
  "Check if a vector contains a value"
  [vec val]
  (some (partial = val) vec))

(defn function-name?
  "Resolves true if a string or symbol resolves into a function name"
  [config]
  (some? (resolve (symbol config))))

(defn read-json
  "Reads the function maing json and spits out a map of aliases to functions"
  [file-location]
  (json/read-str (slurp file-location)))

(defn write-json
  "Takes a map and writs json to a file"
  [file-location mapin]
  (spit file-location (json/write-str mapin)))

(defn remove-chars-end
  "Remove the last character of a string if it contains any of these characters"
  [r-str end-chars]
  (if (not-empty end-chars)
   (if (= (first end-chars) (last r-str))
    (subs r-str 0 (- (count r-str) 1))
    (remove-chars-end r-str (rest end-chars)))
   r-str))

(defn remove-end-chars
  "Remove special characters from a function name so it can be interpreted"
  [r-str end-chars]
  (let [next-r-str (remove-chars-end r-str end-chars)]
    (if (or (= next-r-str "") (= next-r-str r-str))
      r-str
      (remove-end-chars next-r-str end-chars))))

(defn partial-apply
  "Combines partial and apply into one"
  [func-list params]
  (let [formatted-params (if (map? params)
                           (mapcat identity params)
                           params)]
  (apply (apply partial func-list) formatted-params)))

(defn make-sure-sequence
  "If not a sequence, return a sequence"
  [x]
  (if (sequential? x)
    x
    [x]))

(defn remove-single-quotes
  "Remove single quotes from the string"
  [s]
  (clojure.string/replace s #"'" ""))

(defn prepend-str
  "Prepend a string to another string"
  [pre str-to-pre]
  (str pre str-to-pre))

(defn prepend-str-if-not-there
  "Prepend of a string only if the substring is not already at the beginning"
  [pre str-to-pre]
  (let [str-to-pre-t (clojure.string/trim str-to-pre)
        str-beginning (subs str-to-pre-t 0 (- (count pre) 1))]
    (if (= pre str-to-pre)
      str-to-pre-t
      (prepend-str pre str-to-pre-t))))  

(defn trim-protocol
  "Trim the 'http://', 'https://', and other xxxx:// protocols from the string"
  [url]
  (let [trimmed-string (re-find #"://(.*)" url)]
    (if trimmed-string
      (last trimmed-string)
      url)))

(defn error?
  "Did this function throw an error?"
  [f & args]
  (try (apply f args)
       (catch Exception e nil)))

(defn unsigned-to-signed-byte
  [byte]
  (let [int-byte (int byte)]
    (- int-byte 128)))

(defn signed-to-unsigned-byte
  [byte]
  (let [int-byte (int byte)]
    (if (< byte 0)
      (+ int-byte 256)
      int-byte)))

;;Due to the fact that (insanely) bytes are signed whereas chars are unsigned
;;we have to do gymnastics like above and below to line things up properly
;;Also, a char can be coerced to a byte but not an unchecked-byte
;;even though unchecked-byte returns a byte...
;;Yeah I don't know either. That's why we convert it to an int and then an unchecked-byte
(defn string-to-bytes
  [string]
  (byte-array (seq (map (comp unchecked-byte int) string))))

(defn bytes-to-string
  "Convert bytes to a string. Supports signed and unsigned bytes."
  [byte-arr]
  (apply str (map (comp char signed-to-unsigned-byte) byte-arr)))

(defn insecure-random-char
  "Insecure random number, prioritizing speed over security"
  []
  (char (rand-int 255)))

(defn insecure-random-str
  "Inscure random string, prioritizing speed over security"
  [size]
  (loop [n 0
         ran-string ""]
    (if (= n size)
      ran-string
      (recur (inc n)
             (str ran-string (insecure-random-char))))))

(defn fill-garbage
  "Fill string with garbage, maxing at size"
  [prepend size]
  (let [fill-size (- size (count prepend))]
       (str prepend (insecure-random-str fill-size))))


(defn get-user-input
  "Gets a line from the user in the terminal"
  [prepend]
  (do (println prepend)
      (read-line)))

(defn get-user-password
  "Gets a line form the user in the terminal, hiding the input text"
  [prepend]
  (if (System/console)
    (do (println prepend)
        (.readPassword (System/console)))
    (do (println "Warning! Cannot connect to special console interface, reverting to basic input. Typed password will be revealed in plaintext")
        (println prepend)
        (.readLine (java.io.BufferedReader. *in*)))))

(defn wait-for-confirmation
  "Have a y/n blocking terminal prompt"
  [prepend]
  (do (print prepend)
      (println " Proceed? (y/n)")
      (let [answer (read-line)]
        (if (= answer (clojure.string/lower-case answer) "n")
          (do (println "ABORTING EXECUTION")
              (System/exit -1))
          (when (not= answer (clojure.string/lower-case answer) "y")
            (do (println "Unknown answer, please answer 'y' for yes or 'n' for no.")
                (recur prepend)))))))

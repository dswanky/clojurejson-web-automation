(ns automate_shipping.web_automation
  (:gen-class))

(use '[automate_shipping.utility])
(use '[automate_shipping.aes])
(use '[etaoin.api])
(use '[clojure.java.shell :only [sh]])
(use '[flatland.ordered.map])
(require '[etaoin.keys :as k])

(defn launch-driver
  "Launch the driver to connect to on another thread"
  [driver & args]
  (future (sh driver args)))

(def browser)

;; I hate to modify variables, so look for a functional alternative.
;; Maybe just allow the user to define variables and pass those as parameters
(defn setup-browser
  "NOTE: INSECURE!!! User can launch any defined function. Please sanitize input. Launch the browser. Input can be a string (user input) or a symbol (internal)"
  [browser-name host port]
    (def browser ((resolve (symbol browser-name)) {:host host :port port})))

(defn open-site
  "Opens a link in the connected browser"
  [link]
  (go browser link))


(defn process-query-tag
  "Process a query tag as either a string or a keyword"
  [t]
  (if (= (first t) \')
    (remove-single-quotes t)
    (keyword t)))

(defn process-query-tags-map-s
  "Convert JSON to the etaoin map query syntax"
  [tag-map]
  (apply ordered-map (map process-query-tag tag-map)))

(defn process-query-tags-vec-s
  "Convert JSON to the etaoin vector query syntax"
  [tag-vec]
  (map (fn [x]
         (if (vector? x)
           (process-query-tags-map-s x)
           x)) tag-vec))

(defn process-query-tags
  "Process the JSON query tags into the correct format for etaoin"
  [tags]
  (if (every? (fn [x] (not (vector? x))) tags) ; If every element is not a vector, it is in map format
    (process-query-tags-map-s tags)
    (process-query-tags-vec-s tags)))

(defn wait-element-exists
  "Wait until an element exists on the page"
  [& query]
  (wait-exists browser (process-query-tags query)))

(defn wait-element-visible
  "Wait until an element is visible on the page"
  [& query]
  (wait-visible browser (process-query-tags query)))

(defn wait-seconds
  "Wait n seconds before proceeding"
  [n]
  (wait browser n))

(defn select-option
  "Select a checkbox/option"
  [option-text & query]
  (select browser (process-query-tags query) option-text))

(defn enter-text
  "Enter text into a text box, input, etc"
  [text & query]
  (fill browser (process-query-tags query) text))

(defn get-element-cur-value
  "Gets the current value of an element, such as an input"
  [& query]
  (get-element-value browser (process-query-tags query)))

(defn get-page-url
  "Get the url of the page"
  [& args]
  (get-url browser))

(defn get-page-hostname
  "Get the hostname of the page"
  [& args]
  (re-find #"(?<=:/*)[a-zA-Z0-9\.]+" (get-page-url args)))

(defn get-page-domain
  "Get the domain name of the page"
  [& args]
  (let [domain-items (re-find #"([\w-]+\.)+(\w+)" (get-page-hostname))]
    (str (last (butlast domain-items)) (last domain-items)))); concat second to last and last domain-items

(defn get-page-title
  "Get the title of the page"
  [& args]
  (get-title browser))

(defn get-page-source
  "Get the current browser source of the page"
  [& args]
  (get-source browser))

(defn click-element
  "Emulate a mouse-click on an element"
  [& query]
  (click browser (process-query-tags query)))

(defn press-enter-on
  "Press the enter key"
  [& query]
  (apply (partial enter-text k/enter) query))

;;Another global variable grrrrr
;;Ideas on how to evaluate these functions without global variables are welcome
;; My idea is basically wrapper their execution in a parent function
;; Can execute each element in a row, recursively and pass the variables as inputs
(def window-mapping {})

(defn execute-javascript
  "Execute javascript in the browser"
  [script-text & js-args]
  (js-execute browser script-text js-args))

(defn set-browser-script-timeout
  "Let the browser know how long to execute a script using one of the execute-javascript* functions before timing out"
  [seconds]
  (set-browser-script-timeout browser seconds))

(defn execute-javascript-async
  "Execute javascript in the browser asynchronously"
  [script-text js-args]
  (js-async browser script-text js-args))

(defn open-new-window
  "Opens a new browser window for a given url"
  [name url]
  (do
    (execute-javascript browser "window.open(arguments[0])" url)
    (let [window-handles (get-window-handles browser)]
      (def window-mapping
            (assoc window-mapping name
                   (nth window-handles
                        (- (count window-handles) 1)))))))

(defn switch-to-window
  "Switch browser to a different window"
  [window-alias]
  (switch-to-window browser (get window-mapping window-alias)))

(defn close-browser-window
  "Closes a window in the browser"
  [window-alias]
  (close-window browser (get window-mapping window-alias)))

(defn close-browser
  "Close the browser"
  []
  (quit browser))

(defn http?
  "Check if a site is open to http"
  [url]
  (error? slurp (prepend-str "http://" (trim-protocol url))))

(defn https?
  "Check if a site is open to https"
  [url]
  (error? slurp (prepend-str "https://" (trim-protocol url))))

(defn =-func
  "Java doesn't see = as a function when resolving form a symbol after compiling to a jar, this fixes that"
  [& args]
  (apply = args))

(defn captcha?
  "Does the webpage contain a captcha?"
  [& args]
  (some? (re-find #"(?i)captcha" (get-page-source))))

(defn test1
  ""
  [& args]
  (println "test1 man"))

(defn test2
  ""
  [& args]
  (println "test2 dude"))

(defn test3
  ""
  [& args]
  (println "test3 bro"))
